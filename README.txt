
--------------------------------------------------------------------------------
              Time period widget & formatter for decimal fields
--------------------------------------------------------------------------------

A simple time period widget and formatter for time periods stored as decimals.

Time periods can be entered using multiple units, whereas the module allows
inputing days, hours, minutes, seconds and milliseconds. Each unit can be
enabled separately and the maximum value can be configured.


Dependencies
------------

 * Field API (core)

Usage
-----

 * Enable the module.
 * Create a field of type decimal and select a time period widget and or
   formatter.
   

Maintainers:
 * berliner, berliner@dev030.com
 